module upDownCounter (
	input rst,
	input clk,
	
	output reg [REG_WIDTH-1:0] counter = START_NUMBER,
	output reg directionChange
	
);

	parameter REG_WIDTH = 8;
	parameter START_NUMBER = 0;
	parameter MIN_NUMBER = 0;
	parameter MAX_NUMBER = 255;
	parameter COUNT_SPEED = 1;

	reg counterDirection = 0;

	always @(posedge clk or posedge rst) begin
		if (rst == 1'b1) begin
			counter <= START_NUMBER;
			counterDirection <= 1'b0;
		end
		else begin
			//counter direction
			if(counter == MIN_NUMBER) begin
				counter <= counter + COUNT_SPEED;
				counterDirection <= 1'b0; // +
				directionChange <= 1'b1; //send directionChange signal out
				
			end
			else if(counter == MAX_NUMBER || counter + COUNT_SPEED > MAX_NUMBER) begin
				counter <= counter - COUNT_SPEED;
				counterDirection <= 1'b1; //reverse -
				directionChange <= 1'b1; //send directionChange signal out
			end
			else if(counterDirection == 0 && counter < MAX_NUMBER) begin
				counter <= counter + COUNT_SPEED;
				directionChange <= 1'b0; //not changing directions
			end
			else if (counterDirection == 1 && counter > MIN_NUMBER) begin
				counter <= counter - COUNT_SPEED;
				directionChange <= 1'b0; //not changing directions 
			end
		end
	end


endmodule
