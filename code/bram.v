module bram(
    input R_CLK,
    input W_CLK,
    
    input BRAM_IN,
    output reg BRAM_OUT,
    
    input [10:0] BRAM_ADDR_R,
    input [10:0] BRAM_ADDR_W,
    
    input B_CE_W,
    input B_CE_R
);

    reg mem [4940:0];
    
    always@(posedge R_CLK) begin// reading from RAM sync with system clock 
    if(!B_CE_R)
        BRAM_OUT <= mem[BRAM_ADDR_R];   
    end 
    
    always@(posedge W_CLK) begin// writing to RAM sync with Slave SPI clock.
    if(!B_CE_W)
        mem[BRAM_ADDR_W] <= BRAM_IN;
    end

endmodule