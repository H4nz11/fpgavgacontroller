module top (
	input hwclk,

	input button0,
	//input player1DownButton,
	//input player2UpButton,
	//input player2DownButton,

	output hsync,
	output vsync,
	
	output reg [2:0] red,
	output reg [2:0] green,
	output reg [1:0] blue,

	output led0,
	output led1,
	output lcol0
);

	//PLL 25.13MHz (-0.18% from 25.175MHz optimal)
	
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b1000010),
		.DIVQ(3'b101),
		.FILTER_RANGE(3'b001)
	) pll (
		.REFERENCECLK(hwclk),
		.PLLOUTCORE(clk),
		.LOCK(),
		.RESETB(1'b1),
        .BYPASS(1'b0)
	);

	//horizontal and vertical position
	wire [9:0] sx, sy;

	//reset button
	wire rst = ~button0;
	//wire rst = 0; //reset disable
	wire newFrame;

	//data enable
	wire de;

	vga display_inst (
		.clk(clk),
		.rst(rst),
		.hsync(hsync),
		.vsync(vsync),
		.horizontalCounter(sx),
		.verticalCounter(sy),
		.de(de),
		.led0(led0),
		.led1(led1),
		.lcol0(lcol0),
		.newFrame(newFrame)
	);

	//game localparams
	wire [7:0] lifeColorOut;

	gameOfLife life (
		.clk(clk),
		.frameClk(newFrame),
		.rst(rst),
		.colorOut(lifeColorOut)
	);

	//painting
	always @(clk or rst) begin
		if(rst == 1'b1) begin
			red <= 3'b0;
			green <= 3'b0;
			blue <= 2'b0;
		end
		else begin
			if(de == 1'b1) begin
				//painting (visible)
				red <= lifeColorOut[7:5];
				green <= lifeColorOut[4:2];
				blue <= lifeColorOut[1:0];
			end
			else begin
				//porches
				red <= 3'b0;
				green <= 3'b0;
				blue <= 2'b0;
			end
		end
	end
endmodule
