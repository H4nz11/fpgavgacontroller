module gameOfLife (
	input rst,
	input clk, //pixel clock
	input frameClk, //frame clock

	//screen position (beam position)
	input wire [9:0] sx,
	input wire [9:0] sy,

	output reg[7:0] colorOut //red 3 bits, green 3 bits, blue 2 bits
);


	//top left corner of map
	parameter POSX = 10'b0;
	parameter POSY = 10'b0;

	//map parameters
	parameter MAP_WIDTH = 39;	//????
	parameter MAP_HEIGHT = 29;	//????
	parameter SCALING = 3'd4;	//???
	parameter CELL_SIZE = 16;	//????
	localparam MAP_PADDING = 2'd2;

	//game parameters
	parameter GENERATION_TIME = 6'd60; //new generation every 60 frames
	parameter COLOR_IN = 8'b11111111;//color of cells


	wire [15:0] rData;
	reg [10:0] rAddr;
	reg rClkE;
	reg rE;

	reg [15:0] wData;
	reg [10:0] wAddr;
	reg wClkE;
	reg wE;
	reg [15:0] wMask;


	/*SB_RAM40_4K #(
		.INIT_0(256'h0000000000000000000000000000000000000000000000000000000000000000)
	) ram40_4k (
		.RDATA(rData),
		.RADDR(rAddr),
		.RCLK(clk),
		.RCLKE(rClkE),
		.RE(rE),
		.WADDR(wAddr),
		.WCLK(clk),
		.WCLKE(wClkE),
		.WDATA(wData),
		.WE(wE)
		//.MASK(wMask)
	);*/

	reg dataIn;
	wire dataOut;

	bram mem1(
		.R_CLK(clk),
		.W_CLK(clk),
		.BRAM_IN(dataIn),
		.BRAM_OUT(dataOut),
		.B_CE_W(wClkE),
		.B_CE_R(rClkE)
	);

	always @(posedge clk or posedge rst) begin
		if (rst) begin
			colorOut <= 8'b0;
		end
		else begin
			//tady se bude neco dit
		end
	end

endmodule