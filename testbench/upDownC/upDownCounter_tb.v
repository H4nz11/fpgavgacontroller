//define timescale
`timescale 1ns / 10ps

module upDownCounter_tb ();

	//internal signals
	//wire hsync;
	
	//storage elements
	reg clk = 0;
	reg rst = 0;

	//pres 30 framu
	localparam DURATION = 17000000;

	//generovani clock signalu 25.1xxMHz
	always begin
		#20
		clk = ~clk;
	end

	// uut
	
	upDownCounter #(
	) uut (
		.clk(clk),
		.rst(rst)
	);

	initial begin
		#100
		rst = 1'b1;
		#1
		rst = 1'b0;
	end

	initial begin
		$dumpfile("upDownCounter_tb.vcd");
		$dumpvars(0, upDownCounter_tb);

		#(DURATION)

		$display("Finished!");
		$finish;
	end
		
endmodule
