//define timescale
`timescale 1ns / 10ps

module vga_tb ();

	//internal signals
	//wire hsync;
	
	//storage elements
	reg clk = 0;
	reg rst = 1;

	//pres 30 framu
	localparam DURATION = 17000000;

	//generovani clock signalu 25.1xxMHz
	always begin
		#19.861
		clk = ~clk;
	end

	// uut
	vga #() uut (
		.clk(clk),
		.button0(rst)
	);

	initial begin
		#100
		rst = 1'b0;
		#1
		rst = 1'b1;
	end

	initial begin
		$dumpfile("vga_tb.vcd");
		$dumpvars(0, vga_tb);

		#(DURATION)

		$display("Finished!");
		$finish;
	end
		
endmodule
