module pongLogic (
	input clk, //pixel clock
	input frameClk, //frame clock
	input rst,

	//screen position (beam position)
	input wire [9:0] sx,
	input wire [9:0] sy,

	input wire player1UpButton,
	input wire player1DownButton,
	input wire player2UpButton,
	input wire player2DownButton,


	output reg [REG_WIDTH-1:0] colorOut


);
	//parameters
	parameter REG_WIDTH = 8;
	parameter BALLSIZE = 30;
	parameter BALLSPEED = 2; //pozdeji se může předělat na reg a rychlost zvětšovat
	parameter PADDLESPEED = 2;
	parameter PADDLEWIDTH = 10'd20;
	parameter PADDLEHEIGHT = 100;
	parameter WINSCORE = 5'd9;

	//ball
	reg ballRstSignal = 0;
	wire ballReset = rst || ballRstSignal;

	//upDownCounters
	//X position ball counter
	wire [9:0] counterPosXBall;
	reg xPosBallDirection = 1'b0;
	upDownCounterManual #(
		.REG_WIDTH(10),
		.START_NUMBER(BALLSPEED),
		.MAX_NUMBER(10'd639 - BALLSIZE),
		.COUNT_SPEED(BALLSPEED)
	) upDownXBall (
		.clk(frameClk),
		.rst(ballReset),
		.counterDirection(xPosBallDirection),
		.counter(counterPosXBall)
	);

	//Y position ball counter
	wire [9:0] counterPosYBall;
	wire directionChangeY;
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER(10'd479 - BALLSIZE),
		.COUNT_SPEED(BALLSPEED)
	) upDownYBall (
		.clk(frameClk),
		.rst(ballReset),
		.counter(counterPosYBall),
		.directionChange(directionChangeY)
	);

	//ball drawing
	wire [7:0] colorOutBall;
	drawRectangle #(
		//parameters
		.SIDE_A(BALLSIZE),
		.SIDE_B(BALLSIZE)
	) ball (
		.clk(clk),
		.rst(ballReset),
		.sx(sx),
		.sy(sy),
		.posx(counterPosXBall),
		.posy(counterPosYBall),
		.colorIn(8'b11111111),
		.colorOut(colorOutBall)
	);

	//paddles drawing

	//player1
	//Y position player1 paddle counter
	wire player1Move = frameClk && (~player1UpButton || ~player1DownButton);

	wire [9:0] counterPosYP1;
	wire [7:0] colorOutPlayer1;
	upDownCounterManual #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER (479 - PADDLEHEIGHT),
		.COUNT_SPEED(PADDLESPEED)
	) upDownP1 (
		.clk(player1Move),
		.rst(rst),
		.counterDirection(~player1UpButton),
		.counter(counterPosYP1)
	);

	drawRectangle #(
		//parameters
		.SIDE_A(PADDLEWIDTH),
		.SIDE_B(PADDLEHEIGHT)
	) player1 (
		.clk(clk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.posx(10'b0),
		.posy(counterPosYP1),
		.colorIn(8'b11111111),
		.colorOut(colorOutPlayer1)
	);

	//player2
	//Y position player2 paddle counter
	wire player2Move = frameClk && (~player2UpButton || ~player2DownButton);

	wire [9:0] counterPosYP2;
	wire [7:0] colorOutPlayer2;
	upDownCounterManual #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER (479 - PADDLEHEIGHT),
		.COUNT_SPEED(PADDLESPEED)
	) upDownP2 (
		.clk(player2Move),
		.rst(rst),
		.counterDirection(~player2UpButton),
		.counter(counterPosYP2)
	);

	drawRectangle #(
		//parameters
		.SIDE_A(PADDLEWIDTH),
		.SIDE_B(PADDLEHEIGHT)
	) player2 (
		.clk(clk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.posx(10'd639 - PADDLEWIDTH),
		.posy(counterPosYP2),
		.colorIn(8'b11111111),
		.colorOut(colorOutPlayer2)
	);

	//middle line
	wire [7:0] lineColorOut;
	middleLine #(
		//parameters
	) midLine (
		.clk(clk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.colorOut(lineColorOut)
	);

	//score drawing

	localparam scoreWidth = 3;
	localparam scoreYPos = 10'd80;
	localparam scoreXOffset = 10'd100;

	//player1 score
	wire [7:0] player1ScoreColorOut;
	reg [3:0] player1Score = 4'd0;

	drawScore #(
		//parameters
	) p1Score (
		.clk(clk),
		.clkFrame(frameClk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.posx(10'd319 - scoreXOffset - scoreWidth),
		.posy(scoreYPos),
		.numberIn(player1Score),
		.colorOut(player1ScoreColorOut)
	);

	//player2 score
	wire [7:0] player2ScoreColorOut;
	reg [3:0] player2Score = 4'd0;

	drawScore #(
		//parameters
	) p2Score (
		.clk(clk),
		.clkFrame(frameClk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.posx(10'd319 + scoreXOffset),
		.posy(scoreYPos),
		.numberIn(player2Score),
		.colorOut(player2ScoreColorOut)
	);

	always @(posedge frameClk or posedge rst) begin
		if (rst) begin
			// reset
			ballRstSignal <= 0;
		end
		else begin
			//logika odrazu od palky
			if(((counterPosXBall + BALLSIZE) >= (10'd639 - PADDLEWIDTH)) && ((counterPosXBall + BALLSIZE) <= (10'd639 - PADDLEWIDTH+BALLSPEED)) && counterPosYBall + BALLSIZE > counterPosYP2 && counterPosYBall < counterPosYP2 + PADDLEHEIGHT) begin
				//odraz player2
				xPosBallDirection <= 1'b1;
			end
			else if(counterPosXBall <= PADDLEWIDTH && counterPosXBall > PADDLEWIDTH - BALLSPEED && counterPosYBall + BALLSIZE > counterPosYP1 && counterPosYBall < counterPosYP1 + PADDLEHEIGHT) begin
				//odraz player1
				xPosBallDirection <= 1'b0;
			end

			//logika přičítání bodů
			if(counterPosXBall+BALLSIZE == 639) begin
				player1Score <= player1Score + 1;
				ballRstSignal <= 1;
				xPosBallDirection <= 0;
			end
			else if (counterPosXBall == 0) begin
				player2Score <= player2Score + 1;
				ballRstSignal <= 1;
				xPosBallDirection <= 0;
			end
			else begin
				ballRstSignal <= 0;
			end
		end
	end

	always @(posedge clk or posedge rst) begin
		if (rst) begin
			// reset
			
		end
		else begin
			if(colorOutBall != 8'b0) begin
				colorOut <= colorOutBall;
			end
			else begin
				colorOut <= colorOutPlayer1 + colorOutPlayer2 + lineColorOut + player1ScoreColorOut + player2ScoreColorOut;
			end
		end
	end

endmodule