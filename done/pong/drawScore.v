module drawScore (
	input rst,
	input clk, //pixel clock
	input clkFrame,

	input wire [3:0] numberIn,

	//screen position (beam position)
	input wire [9:0] sx,
	input wire [9:0] sy,

	//position of top left corner of text
	input wire [9:0] posx,
	input wire [9:0] posy,

	output reg[7:0] colorOut //red 3 bits, green 3 bits, blue 2 bits
);
	//font
	localparam [textResolution-1:0] number0 = 15'b111_101_101_101_111;
	localparam [textResolution-1:0] number1 = 15'b001_001_001_001_001;
	localparam [textResolution-1:0] number2 = 15'b111_001_111_100_111;
	localparam [textResolution-1:0] number3 = 15'b111_001_111_001_111;
	localparam [textResolution-1:0] number4 = 15'b101_101_111_001_001;
	localparam [textResolution-1:0] number5 = 15'b111_100_111_001_111;
	localparam [textResolution-1:0] number6 = 15'b100_100_111_101_111;
	localparam [textResolution-1:0] number7 = 15'b111_001_001_001_001;
	localparam [textResolution-1:0] number8 = 15'b111_101_111_101_111;
	localparam [textResolution-1:0] number9 = 15'b111_101_111_001_001;

	reg [textResolution-1:0] numberRendered = number0;

	localparam colorIn = 8'b11111111;

	//resolution of numbers
	localparam textWidth = 3;
	localparam textHeight = 5;
	localparam textResolution = textWidth*textHeight;

	always @(posedge clkFrame or posedge rst) begin
		if (rst) begin
			numberRendered <= number0;
		end
		else begin
			//chosing what number is rendered
			case (numberIn)
				4'd0	: numberRendered <= number0;
				4'd1	: numberRendered <= number1;
				4'd2	: numberRendered <= number2;
				4'd3	: numberRendered <= number3;
				4'd4	: numberRendered <= number4;
				4'd5	: numberRendered <= number5;
				4'd6	: numberRendered <= number6;
				4'd7	: numberRendered <= number7;
				4'd8	: numberRendered <= number8;
				4'd9	: numberRendered <= number9;
				default	: numberRendered <= number0;
			endcase
		end
	end

	always @(posedge clk or posedge rst) begin
		if (rst) begin
			colorOut <= 8'b0;
		end
		else begin
			//number drawing
			if(sx >= posx && sy >= posy && sx < posx + textWidth && sy < posy + textHeight) begin
				//here will the text be rendered
				if(numberRendered[textResolution-1-(sx-posx)-(sy-posy)*textWidth] == 1'b1) begin
					colorOut <= colorIn;
				end
				else begin
					colorOut <= 8'b0; //else black
				end
			end
			else begin
				colorOut <= 8'b0; //else black
			end
		end
	end


endmodule
