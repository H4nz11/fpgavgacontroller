module middleLine (
	input rst,
	input clk, //pixel clock

	//screen position (beam position)
	input wire [9:0] sx,
	input wire [9:0] sy,

	output reg[7:0] colorOut //red 3 bits, green 3 bits, blue 2 bits
);
	parameter LINE_WIDTH = 5; //5 px wide

	localparam screenWidth = 639;
	localparam lineColor = 8'b11111111;

	always @(posedge clk or posedge rst) begin
		if (rst == 1'b1) begin
			colorOut <= 8'b0;
		end
		else begin
			if(sx >= 319 - LINE_WIDTH / 2 && sx < 319 + LINE_WIDTH / 2 && sy[3] == 0) begin
				colorOut <= lineColor; //white
			end
			else begin
				colorOut <= 8'b0; //all else black
			end
		end
	end

endmodule