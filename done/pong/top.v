module top (
	input hwclk,

	input player1UpButton,
	input player1DownButton,
	input player2UpButton,
	input player2DownButton,

	output hsync,
	output vsync,
	
	output reg [2:0] red,
	output reg [2:0] green,
	output reg [1:0] blue,

	output led0,
	output led1,
	output lcol0
);

	//PLL 25.13MHz (-0.18% from 25.175MHz optimal)
	
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b1000010),
		.DIVQ(3'b101),
		.FILTER_RANGE(3'b001)
	) pll (
		.REFERENCECLK(hwclk),
		.PLLOUTCORE(clk),
		.LOCK(),
		.RESETB(1'b1),
        .BYPASS(1'b0)
	);

	//horizontal and vertical position
	wire [9:0] sx, sy;

	//reset button
	//wire rst = ~button0;
	wire rst = 0; //reset disable
	wire newFrame;

	//data enable
	wire de;

	vga display_inst (
		.clk(clk),
		.rst(rst),
		.hsync(hsync),
		.vsync(vsync),
		.horizontalCounter(sx),
		.verticalCounter(sy),
		.de(de),
		.led0(led0),
		.led1(led1),
		.lcol0(lcol0),
		.newFrame(newFrame)
	);

	//game localparams

	localparam ballSize = 30;
	localparam ballSpeed = 4;
	localparam paddleSpeed = 3;
	wire [7:0] pongColorOut;

	pongLogic #(
		.BALLSIZE(ballSize),
		.BALLSPEED(ballSpeed),
		.PADDLESPEED(paddleSpeed)
	) pongGame (
		.clk(clk),
		.frameClk(newFrame),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.player1UpButton(player1UpButton),
		.player1DownButton(player1DownButton),
		.player2UpButton(player2UpButton),
		.player2DownButton(player2DownButton),
		.colorOut(pongColorOut)
	);

	//painting
	always @(clk or rst) begin
		if(rst == 1'b1) begin
			red <= 3'b0;
			green <= 3'b0;
			blue <= 2'b0;
		end
		else begin
			if(de == 1'b1) begin
				//painting (visible)

				red <= pongColorOut[7:5];
				green <= pongColorOut[4:2];
				blue <= pongColorOut[1:0];
			end
			else begin
				//porches
				red <= 3'b0;
				green <= 3'b0;
				blue <= 2'b0;
			end
		end
	end
endmodule
