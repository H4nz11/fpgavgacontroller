module upDownCounterManual (
	input rst,
	input clk,
	input wire counterDirection,
	
	output reg [REG_WIDTH-1:0] counter = START_NUMBER
);

	parameter REG_WIDTH = 8;
	parameter START_NUMBER = 0;
	parameter MIN_NUMBER = 0;
	parameter MAX_NUMBER = 255;
	parameter COUNT_SPEED = 1;

	always @(posedge clk or posedge rst) begin
		if (rst == 1'b1) begin
			counter <= START_NUMBER;
		end
		else begin
			//basic counting
			if(counterDirection == 0) begin
				counter <= counter + COUNT_SPEED;
			end
			else if (counterDirection == 1) begin
				counter <= counter - COUNT_SPEED;
			end

			//sticking to end
			if(counter + COUNT_SPEED > MAX_NUMBER && counterDirection == 0) begin
				counter <= MAX_NUMBER;
			end
			else if (counter - COUNT_SPEED > counter && counterDirection == 1) begin
				counter <= MIN_NUMBER;
			end
		end
	end


endmodule
