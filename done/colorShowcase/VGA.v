module vga (
	input hwclk,
	input button0,
	
	output clkOut,
	
	output led0,
	output reg led1,
	output lcol0,
	
	output reg hsync,
	output reg vsync,
	
	output reg [2:0] red,
	output reg [2:0] green,
	output reg [1:0] blue
);
	//horizontal
	localparam horziontalSpace = 10'd800;
	localparam horizontalVisible = 10'd640;
	localparam horizontalFrontPorch = 5'd16;
	localparam horizontalBackPorch = 6'd48;
	localparam hSyncLength = 7'd96;
	
	//vertical
	localparam verticalSpace = 10'd525;
	localparam verticalVisible = 9'd480;
	localparam verticalFrontPorch = 4'd10;
	localparam verticalBackPorch = 6'd33;
	localparam vSyncLength = 2'd2;
	
	//counters neboli x a y pozice
	reg [9:0] horizontalCounter = 10'b0;
	reg [9:0] verticalCounter = 10'b0;
	
	//ledky
	assign lcol0 = 1'b0;
	
	//button
	wire rst = ~button0;
	assign led0 = button0;
	
	//testing
	localparam frameRate = 10'd60;
	reg [5:0] frameCounter = 6'b0;
	
	//PLL clock out for testing
	wire clkOut = clk;
	
	//PLL 25.13MHz (-0.18% od 25.175MHz)
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b1000010),
		.DIVQ(3'b101),
		.FILTER_RANGE(3'b001)
	) pll (
		.REFERENCECLK(hwclk),
		.PLLOUTCORE(clk),
		.LOCK(),
		.RESETB(1'b1),
        .BYPASS(1'b0)
	);
	
	//pridat reset button
	always @(posedge clk or posedge rst) 
	begin
		if(rst == 1'b1) begin
			verticalCounter <= 10'b0;
			horizontalCounter <= 10'b0;
			
			//testing
			frameCounter <= 6'b0;
		end
		else begin
			if(horizontalCounter == horziontalSpace - 1) begin
			
				if(verticalCounter == verticalSpace - 1) begin
					//end of frame
					verticalCounter <= 10'b0;
					
					//testing aka frame counter
					frameCounter <= frameCounter + 1;
					if(frameCounter == frameRate - 1) begin
						frameCounter <= 6'b0;
						led1 <= 1'b0; //sviti
					end
					else begin
						led1 <= 1'b1; //nesviti
					end
					//end of testing
					
				end
				else begin
					//new radek
					verticalCounter <= verticalCounter + 1;
				end
				horizontalCounter <= 10'b0;	
			end
			else begin
				horizontalCounter <= horizontalCounter + 1;
			end
		end
	end

	always @(horizontalCounter or rst) begin
			if(rst == 1'b1) begin
				red <= 3'b0;
				green <= 3'b0;
				blue <= 2'b0;
				hsync <= 1'b0;
				vsync <= 1'b0;
			end
			else begin
				//hsync
				if((horizontalCounter > horizontalVisible + horizontalFrontPorch - 1) && (horizontalCounter < horizontalVisible + horizontalFrontPorch + hSyncLength)) begin
					hsync <= 1'b0;
				end
				else begin
					hsync <= 1'b1;
				end
				//vsync
				if((verticalCounter > verticalVisible + verticalFrontPorch - 1) && (verticalCounter < verticalVisible + verticalFrontPorch + vSyncLength)) begin
					vsync <= 1'b0;
				end
				else begin
					vsync <= 1'b1;
				end
					
	
				//vykreslovaní
				if(horizontalCounter < horizontalVisible && verticalCounter < verticalVisible) begin
					//visible area
					//tady se muze neco kreslit
				
					red <= horizontalCounter[6:4];
					green <= verticalCounter[6:4];
					blue <= 2'b00;
					
				end
				else begin
					//porches
					red <= 3'b0;
					green <= 3'b0;
					blue <= 2'b0;
				end
			end
		end
	
endmodule
