module top (
	input hwclk,
	input button0,

	output hsync,
	output vsync,
	
	output reg [2:0] red,
	output reg [2:0] green,
	output reg [1:0] blue,

	output led0,
	output led1,
	output lcol0
		
	
);

	//PLL 25.13MHz (-0.18% from 25.175MHz optimal)
	
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b1000010),
		.DIVQ(3'b101),
		.FILTER_RANGE(3'b001)
	) pll (
		.REFERENCECLK(hwclk),
		.PLLOUTCORE(clk),
		.LOCK(),
		.RESETB(1'b1),
        .BYPASS(1'b0)
	);

	//horizontal and vertical position
	wire [9:0] sx, sy;

	//reset button
	wire rst = ~button0;
	wire newFrame;

	//data enable
	wire de;

	vga display_inst (
		.clk(clk),
		.rst(rst),
		.hsync(hsync),
		.vsync(vsync),
		.horizontalCounter(sx),
		.verticalCounter(sy),
		.de(de),
		.led0(led0),
		.led1(led1),
		.lcol0(lcol0),
		.newFrame(newFrame)
	);

	//updown counters

	//red stripe position
	wire [9:0] counterRed;
	
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER(479 - stripeWidth),
		.COUNT_SPEED(stripeSpeed)
	) upDownRed (
		.clk(newFrame),
		.rst(rst),
		.counter(counterRed)
	);

	//green stripe position
	wire [9:0] counterGreen;
	
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(100),
		.MAX_NUMBER (479 - stripeWidth),
		.COUNT_SPEED(stripeSpeed)
	) upDownGreen (
		.clk(newFrame),
		.rst(rst),
		.counter(counterGreen)
	);

	//blue stripe position
	wire [9:0] counterBlue;
	
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(200),
		.MAX_NUMBER (479 - stripeWidth),
		.COUNT_SPEED(stripeSpeed)
	) upDownBlue (
		.clk(newFrame),
		.rst(rst),
		.counter(counterBlue)
	);

	//local params
	localparam stripeWidth = 50;
	localparam stripeSpeed = 1;
	
	//painting
	always @(sx or rst) begin
		if(rst == 1'b1) begin
			red <= 3'b0;
			green <= 3'b0;
			blue <= 2'b0;
		end
		else begin
			if(de == 1'b1) begin
				//painting (visible)

				//red stripe				
				if (sy > counterRed && sy <= stripeWidth + counterRed) begin
					red <= 3'b111;
				end
				else begin
					red <= 3'b000;
				end

				//green stripe
				if (sy > counterGreen && sy <= stripeWidth + counterGreen) begin
					green <= 3'b111;
				end
				else begin
					green <= 3'b000;
				end

				//blue stripe
				if (sy > counterBlue && sy <= stripeWidth + counterBlue) begin
					blue <= 2'b11;
				end
				else begin
					blue <= 2'b00;
				end

				
			end
			else begin
				//porches
				red <= 3'b0;
				green <= 3'b0;
				blue <= 2'b0;
			end
		end
	end
endmodule
