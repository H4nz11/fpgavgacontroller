module vga (
	input clk,
	input rst,
	
	output led0,
	output reg led1,
	output lcol0,
	
	output reg hsync,
	output reg vsync,

	output reg [9:0] horizontalCounter = 10'b0,
	output reg [9:0] verticalCounter = 10'b0,

	output reg newFrame,
	output reg de = 1'b0
);
	//horizontal
	localparam horziontalSpace = 10'd800;
	localparam horizontalVisible = 10'd640;
	localparam horizontalFrontPorch = 5'd16;
	localparam horizontalBackPorch = 6'd48;
	localparam hSyncLength = 7'd96;
	
	//vertical
	localparam verticalSpace = 10'd525;
	localparam verticalVisible = 9'd480;
	localparam verticalFrontPorch = 4'd10;
	localparam verticalBackPorch = 6'd33;
	localparam vSyncLength = 2'd2;
	
	//ledky
	assign lcol0 = 1'b0;
	
	//reset button led
	assign led0 = ~rst;
	
	//frameCounter
	localparam frameRate = 10'd60;
	reg [5:0] frameCounter = 6'b0;
	
	//pridat reset button
	always @(posedge clk or posedge rst) 
	begin
		if(rst == 1'b1) begin
			verticalCounter <= 10'b0;
			horizontalCounter <= 10'b0;

			newFrame <= 1'b0;
			//testing
			frameCounter <= 6'b0;
		end
		else begin
			
			if(horizontalCounter == horziontalSpace - 1) begin
			
				if(verticalCounter == verticalSpace - 1) begin
					//end of frame
					verticalCounter <= 10'b0;
					newFrame <= 1'b1;
					
					//testing aka frame counter
					frameCounter <= frameCounter + 1;
					if(frameCounter == frameRate - 1) begin
						frameCounter <= 6'b0;
						led1 <= 1'b0; //sviti
					end
					else begin
						led1 <= 1'b1; //nesviti
					end
					//end of testing
					
				end
				else begin
					//new radek
					newFrame <= 1'b0;
					verticalCounter <= verticalCounter + 1;
				end
				horizontalCounter <= 10'b0;	
			end
			else begin
				horizontalCounter <= horizontalCounter + 1;
				
			end
		end
	end

	always @(horizontalCounter or rst) begin
			if(rst == 1'b1) begin
				hsync <= 1'b0;
				vsync <= 1'b0;
				de <= 1'b0;
			end
			else begin
				//hsync
				if((horizontalCounter > horizontalVisible + horizontalFrontPorch - 1) && (horizontalCounter < horizontalVisible + horizontalFrontPorch + hSyncLength)) begin
					hsync <= 1'b0;
				end
				else begin
					hsync <= 1'b1;
				end
				//vsync
				if((verticalCounter > verticalVisible + verticalFrontPorch - 1) && (verticalCounter < verticalVisible + verticalFrontPorch + vSyncLength)) begin
					vsync <= 1'b0;
				end
				else begin
					vsync <= 1'b1;
				end
				
				//vykreslovaní
				if(horizontalCounter < horizontalVisible && verticalCounter < verticalVisible) begin
					//visible area
					//tady se muze neco kreslit
					de <= 1'b1;
				end
				else begin
					//porches
					de <= 1'b0;
				end
			end
		end
	
endmodule
