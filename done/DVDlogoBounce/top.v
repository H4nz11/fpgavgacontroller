module top (
	input hwclk,
	input button0,

	output hsync,
	output vsync,
	
	output reg [2:0] red,
	output reg [2:0] green,
	output reg [1:0] blue,

	output led0,
	output led1,
	output lcol0
);

	//PLL 25.13MHz (-0.18% from 25.175MHz optimal)
	
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b1000010),
		.DIVQ(3'b101),
		.FILTER_RANGE(3'b001)
	) pll (
		.REFERENCECLK(hwclk),
		.PLLOUTCORE(clk),
		.LOCK(),
		.RESETB(1'b1),
        .BYPASS(1'b0)
	);

	//horizontal and vertical position
	wire [9:0] sx, sy;

	//reset button
	wire rst = ~button0;
	wire newFrame;

	//data enable
	wire de;

	vga display_inst (
		.clk(clk),
		.rst(rst),
		.hsync(hsync),
		.vsync(vsync),
		.horizontalCounter(sx),
		.verticalCounter(sy),
		.de(de),
		.led0(led0),
		.led1(led1),
		.lcol0(lcol0),
		.newFrame(newFrame)
	);

	//updown counters

	localparam counterSpeed = 1;
	
	//X position counter
	wire [9:0] counterPosX;
	wire directionChangeX;
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER (639 - sideA),
		.COUNT_SPEED(counterSpeed)
	) upDownX (
		.clk(newFrame),
		.rst(rst),
		.counter(counterPosX),
		.directionChange(directionChangeX)
	);

	//Y position counter
	wire [9:0] counterPosY;
	wire directionChangeY;
	upDownCounter #(
		.REG_WIDTH(10),
		.START_NUMBER(0),
		.MAX_NUMBER (479 - sideB),
		.COUNT_SPEED(counterSpeed)
	) upDownY (
		.clk(newFrame),
		.rst(rst),
		.counter(counterPosY),
		.directionChange(directionChangeY)
	);

	//local params
	localparam sideA = 200;
	localparam sideB = 200;
	localparam positionX = 10'd100;
	localparam positionY = 10'd100;

	wire [7:0] colorOutRectangle1;

	drawRectangle #(
		//parameters
		.SIDE_A(sideA),
		.SIDE_B(sideB)
	) rectangle1 (
		.clk(clk),
		.rst(rst),
		.sx(sx),
		.sy(sy),
		.posx(counterPosX),
		.posy(counterPosY),
		.colorIn(randomColor1),
		.colorOut(colorOutRectangle1)
	);
	
	wire randomColorSelect = directionChangeX | directionChangeY; //1 pokud se odrazí od stěny
	wire [7:0] randomColor1;

	randomColor #(
		//using default parameter values
	) randomColorPicker (
		.clk(clk),
		.rst(rst),
		.colorOut(randomColor1),
		.select(randomColorSelect)
	);

	//todo udělat module, kterej bude generovat "náhodnou barvu"
	//aka bude tam 8 bit counter, kterej bude přičítanej clk
	//a bude tam output color, kterej se aktualizuje jenom při vstupnim signalu něco

	//painting
	always @(clk or rst) begin
		if(rst == 1'b1) begin
			red <= 3'b0;
			green <= 3'b0;
			blue <= 2'b0;

			//colorRectangle1 <= 8'b11111111;
		end
		else begin
			if(de == 1'b1) begin
				//painting (visible)
				
				red <= colorOutRectangle1[7:5];
				green <= colorOutRectangle1[4:2];
				blue <= colorOutRectangle1[1:0];
			end
			else begin
				//porches
				red <= 3'b0;
				green <= 3'b0;
				blue <= 2'b0;
			end
		end
	end
endmodule
