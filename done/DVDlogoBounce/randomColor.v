module randomColor (
	input clk,
	input rst,
	input select,

	output reg[REG_WIDTH-1:0] colorOut

);

	parameter REG_WIDTH = 8;
	wire directionChange;

	wire [REG_WIDTH-1:0] colorCounter;

	wire clockUpdate = clk && ~select; //clock enable only when slect is 0

	upDownCounter #(
		//default 8 bit up down counter
	) counterYeet (
		.clk(clockUpdate),
		.rst(rst),
		.counter(colorCounter),
		.directionChange(directionChange)
	);

	always @(posedge clk or posedge rst) begin
		if (rst) begin
			colorOut <= 0;
		end
		else begin
			if(select == 1) begin //1 means this random color to colorOut
				colorOut <= colorCounter;
			end
		end
	end

endmodule