module drawRectangle  (
	input rst,
	input clk, //pixel clock

	input wire [7:0] colorIn, //red 3 bits, green 3 bits, blue 2 bits

	//screen position (beam position)
	input wire [9:0] sx,
	input wire [9:0] sy,

	//position of top left corner of rectangle
	input wire [9:0] posx,
	input wire [9:0] posy,

	output reg[7:0] colorOut //red 3 bits, green 3 bits, blue 2 bits
);


	parameter SIDE_A = 10; //horizontal side of 10 px
	parameter SIDE_B = 10; //vertical side of 10 px

	always @(posedge clk or posedge rst) begin
		if (rst == 1'b1) begin
			colorOut <= 8'b0;
		end
		else begin
			if(sx >= posx && sx < posx + SIDE_A && sy >= posy && sy < posy + SIDE_B) begin
				colorOut <= colorIn; //white
			end
			else begin
				colorOut <= 8'b0; //all else black
			end
		end
	end

endmodule